from models.account import Account
from models.atm import ATM
from models.bank import Bank

from models.card import Card
from models.client import Client

#bank
bank = Bank()

#client - name, client id
clt1 = Client("cesar", 111111)
#account - account id, client, initial balance, account options
ac1 = Account(333333, clt1, 1000, [])
bank.add_account(ac1)
#card - client id, account id, card id
card1 = Card(111111, 333333, 555555)
#pin link - card id, pin
bank.create_pin(555555, 1234)

clt2 = Client("andre", 222222)
ac2 = Account(444444, clt2, 2000, [])
bank.add_account(ac2)
card2 = Card(222222, 444444, 666666)
bank.create_pin(666666, 4321)

atm = ATM(bank)
atm.read_card(card1)
