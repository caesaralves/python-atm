from typing import Set

import models.client as Client
from models.option import OptionType


class Account:
    def __init__(self, accountnumber: int, client: Client, balance: float, options: Set[OptionType]):
        self.client = client
        self.balance = balance
        self.options = options
        self.accountnumber = accountnumber

    def remove_from_balance(self, amount: float) -> float:
        tempcalc = self.balance - amount
        if tempcalc < 0:
            raise Exception('Insufficient funds.')
        self.balance = tempcalc
        print(f'Account:{self.accountnumber} ->  Balance:{self.balance}')
        return self.balance

    def add_to_balance(self, amount: float) -> float:
        assert isinstance(amount, float)
        self.balance += amount
        print(f'Account:{self.accountnumber} ->  Balance:{self.balance}')

    def __repr__(self):
        return f'Account:{self.accountnumber} | client:{self.client.name,} | balance: {self.balance}'