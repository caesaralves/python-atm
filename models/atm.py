from models.account import Account
from models.bank import Bank
from models.card import Card
from models.option import OptionType


class ATM:
    current_account: Account = None

    def __init__(self, bank: Bank):
        self.bank = bank

    def read_card(self, card: Card) -> None:
        self.assert_account_is_clear()

        pin = int(input("Insert pin:"))

        self.load_account(card, pin)
        if self.account_is_loaded():
            self.__show_options()

        self.clear_account()

    def load_account(self, card, pin):
        self.current_account = self.bank.get_client_account(card, pin)

    def __show_options(self):
        #options = self.currentaccount.options
        while True:
            option = int(input(f'Choose one option number:{OptionType.list_options()}'))
            if OptionType.has_value(option):
                self.exec_option(OptionType(option))
            else:
                print("invalid option!")

    def exec_option(self, option: OptionType):
        func = self.options[option]
        func(self)

    def __withdraw(self, amount=None) -> float:
        if not amount:
            amount = float(input("Amount to withdraw:"))
        try:
            self.current_account.remove_from_balance(amount)
        except Exception as err:
            print(err)
        return self.current_account.balance

    def __deposit(self, amount=None) -> float:
        if not amount:
            amount = float(input("Amount to deposit:"))
        self.current_account.add_to_balance(amount)
        return self.current_account.balance

    def __transfer(self) -> float:
        amount = self.__get_amount("Amount to transfer:")
        to_account_number: int = int(input("Enter destination account number:"))
        try:
            self.__withdraw(amount)
        except Exception as err:
            print(err)
        # need to ask bank to deposit on account since cant return an account from someone else
        self.bank.deposit_amount_on_account(to_account_number, amount)
        return self.current_account.balance

    def __exit(self):
        print('Gracias, bye')
        exit()

    @staticmethod
    def __get_amount(message: str) -> float:
        return float(input(f'{message}'))

    def clear_account(self):
        self.current_account = None

    options = {
                OptionType.WITHDRAW: __withdraw,
                OptionType.DEPOSIT: __deposit,
                OptionType.TRANSFER: __transfer,
                OptionType.EXIT: __exit
            }

    def assert_account_is_clear(self):
        try:
            self.current_account
        except NameError:
            print("Error: previous account was not released")
        finally:
            self.clear_account()

    def account_is_loaded(self) -> bool:
        if self.current_account is not None:
            print("Initializing....")
            return True
        else:
            print("Card or pin are invalid")
            return False
