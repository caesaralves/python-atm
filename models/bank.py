from models.account import Account
from typing import Set, Dict

from models.card import Card


class Bank:
    accounts: Set[Account] = set()
    cards_pin: Dict[int, int] = {}  # Dict[cardnumber, pin]

    def __init__(self):
        print('Bank is now open')

    def add_account(self, account):
        assert isinstance(account, Account)
        self.accounts.add(account)
        print(f'Account:{account} created on bank')

    def create_pin(self, cardnumber: int, pin: int):
        self.cards_pin[cardnumber] = pin
        print(self.cards_pin.items())

    def get_client_account(self, card: Card, pin: int) -> Account:

        return next((account
                    for account in self.accounts
                    if self.account_matches(account, card, pin)), None)

    def pin_matches(self, cardnumber: int, pin: int) -> bool:
        return self.cards_pin[cardnumber] == pin

    def account_matches(self, account: Account, card: Card, pin: int):
        return account.accountnumber == card.accountnumber and self.pin_matches(card.cardnumber, pin)

    def deposit_amount_on_account(self, accountnumber: int, amount: float):
        account = next(account
                    for account in self.accounts if (account.accountnumber == accountnumber))
        account.add_to_balance(amount)
