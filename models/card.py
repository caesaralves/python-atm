class Card:
    def __init__(self, cltid: int, accountnumber: int, cardnumber: int) -> None:
        self.cltid = cltid
        self.accountnumber = accountnumber
        self.cardnumber = cardnumber