from enum import Enum, auto


class OptionType(Enum):
    EXIT = auto()
    WITHDRAW = auto()
    DEPOSIT = auto()
    TRANSFER = auto()

    @classmethod
    def has_name(cls, name):
        return any(name == item.name for item in cls)

    @classmethod
    def has_value(cls, value):
        return any(value == item.value for item in cls)

    @classmethod
    def list_options(cls):
        a = ''
        for item in cls:
            a += f'{item.value} - {item.name} \n'
        print(a)
